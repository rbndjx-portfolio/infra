terraform {
  required_version = ">=1.4.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=5.0.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      "Project" = "portfolio"
    }
  }
}

module "frontend" {
  source = "./modules/frontend"

  aws_s3_frontend_bucket_name = var.aws_s3_frontend_bucket_name
}

module "vpc" {
  source = "./modules/vpc"
}

module "ecr" {
  source = "./modules/ecr"

  aws_ecr_backend_repository_name = var.aws_ecr_backend_repository_name
}

module "backend" {
  source = "./modules/backend"

  aws_ecr_repository  = module.ecr.ecr_repository
  ssh_ec2_key_name    = var.ssh_ec2_key_name
  ssh_ec2_private_key = var.ssh_ec2_private_key
  aws_region          = var.aws_default_region
}