variable "aws_s3_frontend_bucket_name" {
  description = "The name of the S3 bucket to create for storing the front-end application"
  type        = string
}