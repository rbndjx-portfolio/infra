variable "aws_ecr_repository" {
  description = "The ECR repository object"
}

variable "backend_port" {
  description = "The port on which the backend application listens"
  type        = string
  default     = "3000"
}

variable "ssh_ec2_private_key" {
  description = "The private key to use to connect to the EC2 instance"
  type        = string
}

variable "ssh_ec2_key_name" {
  description = "The name of the SSH key to use for the EC2 instance"
  type        = string
}

variable "aws_region" {
  description = "The AWS region to use"
  type        = string
}