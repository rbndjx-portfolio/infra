resource "aws_ecr_repository" "portfolio" {
  name         = var.aws_ecr_backend_repository_name
  force_delete = true

  image_scanning_configuration {
    scan_on_push = true
  }
}