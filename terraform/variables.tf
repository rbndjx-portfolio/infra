variable "aws_s3_frontend_bucket_name" {
  description = "The name of the S3 bucket to create for storing the front-end application"
  type        = string
}

variable "ssh_ec2_key_name" {
  description = "The name of the SSH key to use for connecting to the EC2 instance"
  type        = string
}

variable "ssh_ec2_private_key" {
  description = "The private key to use for connecting to the EC2 instance"
  type        = string
}

variable "aws_ecr_backend_repository_name" {
  description = "The name of the ECR repository to create for storing the back-end application"
  type        = string
}

variable "aws_default_region" {
  description = "The AWS region to use"
  type        = string
}